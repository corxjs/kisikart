class UI {
    constructor() {
        console.warn("UI nesnesi oluşturuldu");
    }
    kisiKaydet(kisi) {
        this.cardOlustur(kisi.isim,kisi.url,kisi.bio,kisi.tarih);
    }
    tumAlanlariTemizle(){

    }
    mesajGoster(tip="danger",mesaj){
        document.getElementById("submit").parentElement.innerHTML += `<div id="alert" class="alert alert-${tip}" role="alert">
        <h4 class="alert-heading">${mesaj}</h4>
      </div>`;
        setTimeout(() => {
            document.getElementById("alert").remove();
        },2000);
    }
    cardOlustur(isim, url, bio, tarih) {
        const cardBody = document.getElementsByClassName("card-body")[0];
        /* 
           <div class="card col-3" style="width: 18rem;">
            <img class="card-img-top" src="https://pbs.twimg.com/profile_images/936627850152677376/lzLdERfp_400x400.jpg">
            <div class="card-body">
              <h5 class="card-title">Mustafa Çor</h5>
              <p class="card-text">57 yaşında emekli albay</p>
            </div>
            <ul class="list-group list-group-flush">
             <li class="list-group-item">31.07.1999</li>
            </ul>
            <div class="card-body">
              <a href="#" class="btn btn-danger col-12">Sil</a>
            </div>
          </div> 
        */
        const cardDiv = document.createElement("div");
        cardDiv.className = "card col-3";
        cardDiv.setAttribute("style", "width:18rem");
        const img = document.createElement("img");
        img.className = "card-img-top";
        img.setAttribute("src", url)
        cardDiv.appendChild(img);
        const inLineCardBody = document.createElement("div");
        inLineCardBody.className = "card-body";
        const cardTitle = document.createElement("h5");
        cardTitle.className = "card-title";
        cardTitle.textContent = document.createTextNode(isim).nodeValue;
        const cardBio = document.createElement("p");
        cardBio.className = "card-text";
        cardBio.textContent = document.createTextNode(bio).nodeValue;
        inLineCardBody.appendChild(cardTitle);
        inLineCardBody.appendChild(cardBio);
        cardDiv.appendChild(inLineCardBody);
        //Amelelikten sıkıldım...
        cardDiv.innerHTML += `<ul class="list-group list-group-flush">
       <li class="list-group-item">${tarih}</li>
      </ul>
      <div class="card-body">
        <a href="#" id="sil" onclick="sil(this)" class="btn btn-danger col-12">Sil</a>
      </div>`;
        cardBody.appendChild(cardDiv);
        console.log("ess");
    }
}
